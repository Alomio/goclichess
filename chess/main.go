package chess

import (
	"fmt"
	"log"
)

// Piece A chess Piece
type Piece struct {
	Type  rune
	Value int
	Team  bool
}

//Square a specific square in the game
type Square struct {
	Rank     int
	File     int //Could be a letter but we'll use this for easier navigation
	fileName rune
	Piece    Piece
}

func (s Square) setFileName() {
	if s.File > 7 || s.File < 0 {
		// TODO: Create custom Error to be thrown, invalid square
		log.Fatal("Invalid Square: File is out of bounds")
	}
	var files [8]rune = [8]rune{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'}
	s.fileName = files[s.File]
}

//Board An instance of a chess board
type Board struct {
	Position [64]Square
}

//Print the board to the std out
func (b Board) Print() {
	ranks := [8]int{1, 2, 3, 4, 5, 6, 7, 8}
	files := [8]rune{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'}
	for r := 0; r < 8; r++ {
		var rankRepresentation [8]byte
		for f := 0; f < 8; f++ {
			rankRepresentation[7-f] = byte(b.Position[63-f-(r*8)].Piece.Type)
		}
		fmt.Printf("%d| %s\n", ranks[7-r], string(rankRepresentation[:]))
	}
	fmt.Println("   --------")
	fmt.Printf("   ")
	for fl := 0; fl < 8; fl++ {
		fmt.Printf("%c", files[fl])
	}
	fmt.Printf("\n")
}
