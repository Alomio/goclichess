package chess

import (
	"log"
	"strconv"
	"strings"
)

//Move a piece from one square to another
func Move(from *Square, to *Square) {
	//This is a Niave move right now. Just swapping a piece.
	//TODO Check validatiy of a move
	//TODO Check if capture, promotion, castle
	to.Piece = from.Piece //Oh no, this is going to be a shared memory problem.
	from.Piece = Piece{}
}

//FindSquare - Take a string value and return the integer location within the board
//Of which square it is referring too. a1 = 0, h8=63. Ranks are rows, files are columns
func FindSquare(name string) int {
	//TODO: use regexp to verify the input is letter than number
	//TODO: Probably don't want to fatal an error
	fileMapping := map[string]int{
		"a": 0,
		"b": 1,
		"c": 2,
		"d": 3,
		"e": 4,
		"f": 5,
		"g": 6,
		"h": 7,
	}
	var rank, file string
	arr := strings.Split(name, "")
	if len(arr) != 2 {
		log.Fatalln("Incorrect square name:", name)
	}
	rank, file = arr[1], arr[0]
	rk, err := strconv.Atoi(rank)
	if err != nil {
		log.Fatal("Could not properly read rank", rank, name)
	}

	location := fileMapping[strings.ToLower(file)]*8 + rk - 1

	return location
}

//CreateGame Create a new game on a default Board
func CreateGame() *Board {
	var board Board
	for r := 0; r < 8; r++ {
		for f := 0; f < 8; f++ {
			board.Position[r+f] = Square{
				Rank: r,
				File: f,
				Piece: Piece{
					Type:  '.',
					Value: 0,
				},
			}
		}
	}
	typePosition := [8]rune{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r'}
	for team := 0; team < 2; team++ {
		for f := 0; f < 8; f++ {
			board.Position[(5*team)*8+8+f].Piece = Piece{
				Type:  'p',
				Value: 100,
				Team:  (team == 0),
			}
			switch typePosition[f] {
			case 'r':
				board.Position[(team)*56+f].Piece = Piece{
					Type:  'r',
					Value: 480,
					Team:  (team == 0),
				}
			case 'n':
				board.Position[(team)*56+f].Piece = Piece{
					Type:  'n',
					Value: 280,
					Team:  (team == 0),
				}
			case 'b':
				board.Position[(team)*56+f].Piece = Piece{
					Type:  'b',
					Value: 320,
					Team:  (team == 0),
				}
			case 'q':
				board.Position[(team)*56+f].Piece = Piece{
					Type:  'q',
					Value: 960,
					Team:  (team == 0),
				}
			case 'k':
				board.Position[4+(56*team)].Piece = Piece{
					Type:  'k',
					Value: (960 * 9) + 500, //worth more than 9 queens. Maybe it should be more?
					Team:  (team == 0),
				}
			}
		}
	}
	return &board
}
