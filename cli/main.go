package cli

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"os"

	"lwileczek/gochess/chess"
)

type options struct {
	q string
	m string
	p string
}

//Config A configuration set, possible opitions for setting up CLI chess
type Config struct {
	NewGame   bool
	Color     bool
	TwoPlayer bool
	Position  string
}

//StartUp – Command flags used when starting the CLI
func StartUp() Config {
	newGame := true
	color := true
	twoPlayer := true
	var position string

	//Probably want sub-commands not just flags
	flag.BoolVar(&newGame, "ng", true, "Used to start a new game")
	flag.BoolVar(&color, "black", false, "Used to indicate if white or black")
	flag.BoolVar(&twoPlayer, "two-player", true, "Indicate if two people are playing, false to play against Computer")
	flag.StringVar(&position, "position", "", "Optionally provide a starting position using an FEN")

	flag.Parse()

	c := Config{
		NewGame:   newGame,
		Color:     color,
		TwoPlayer: twoPlayer,
		Position:  position,
	}

	return c
}

//Interactive Interactive cli to communicate with the user
func Interactive(board chess.Board) {
	fmt.Println("Welcome to CLI Chess")
	// I hate this, remove it and make a better print explination
	opts := options{
		q: "Use q to quit",
		m: "Enter m to see this message",
		p: "Use p to print the board",
	}
	var userInput string
	for {
		fmt.Print("Please input a command (m for help): ")
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		if input.Err() != nil {
			fmt.Println("Error reading input from user")
			fmt.Println(input.Err())
			return
		}
		userInput = input.Text()
		switch userInput {
		case "q":
			return
		case "p":
			board.Print()
			break
		case "m":
			fancyStruct, err := json.MarshalIndent(opts, "", "\t")
			if err != nil {
				fmt.Println("Error printing help!")
				fmt.Println(err)
				fmt.Println("Enter 'q' to quit")
			}
			fmt.Println(string(fancyStruct))
			break
		default:
			fmt.Println("Your response was", userInput)
			if len(userInput) == 2 {
				fmt.Println("Looking for a piece at", userInput)
				loc := chess.FindSquare(userInput)
				fmt.Println("That's array position", loc)
				p := board.Position[loc].Piece
				fmt.Printf("We found a %c with Value %d on team: %v\n", p.Type, p.Value, p.Team)
			}
			if len(userInput) == 4 {
				fmt.Println("attempting a move")
				fromPosition := chess.FindSquare(userInput[:2])
				toPosition := chess.FindSquare(userInput[2:4])
				fmt.Printf("going to move from %d to %d\n", fromPosition, toPosition)
				chess.Move(&board.Position[fromPosition], &board.Position[toPosition])
			}
		}
	}
}
