package main

import (
	"lwileczek/gochess/chess"
	cli "lwileczek/gochess/cli"
)

func main() {
	cfg := cli.StartUp()
	if cfg.NewGame {
		var board chess.Board = *chess.CreateGame()
		cli.Interactive(board)
	}
}
